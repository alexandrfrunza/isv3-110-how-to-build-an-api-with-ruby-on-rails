class Api::V1::ProductsController < ApplicationController
  def index
    products = Product.all 
    render json: products, status: 200
  end

  def create
    product = Product.new(prod_params)
     if product.save
      render json: product, status: 200
    else
      render json: {error: "Error creating review."}
    end
  end

  def show
    product = Product.find_by(id: params[:id])
    if product
      render json: product, status: 200
    else
      render json: {error: "Product Not Found."}
    end
  end

  def update 
    product = Product.find_by(id: params[:id])
    product.update(prod_params)
    render json: product, status: 200
  end

  def destroy
    product = Product.find_by(id: params[:id])
    product.destroy
    render json: product, status: 200
  end

  private
  def prod_params
    params.permit([
      :name,
      :brand,
      :price,
      :description
    ])
  end
end
