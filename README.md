# ISV3-110 How to Build an API With Ruby on Rails

API with list of cars.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-110-how-to-build-an-api-with-ruby-on-rails/-/archive/ISV3-110-APi+POSTGRES+DOCKER/isv3-110-how-to-build-an-api-with-ruby-on-rails-ISV3-110-APi+POSTGRES+DOCKER.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```

create database

```bash
docker-compose exec app rake db:create
```

run migrations

```bash
docker-compose exec app rake db:migrate
```

add seeds

```bash
docker-compose exec app rails db:seed
```
## Using api


Find in unziped folder file:

```bash
howtobuild.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 